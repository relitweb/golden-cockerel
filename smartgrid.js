const smartgrid = require('smart-grid');

const settings = {
    outputStyle: 'scss',
    columns: 12,
    offset: '18px',
    container: {
        maxWidth: '1366px',
        fields: '9px'
    },
    breakPoints: {
        lg: {
            width: "1280px",
            fields: "9px"
        },
        md: {
            width: "992px",
            fields: "9px"
        },
        sm: {
            width: "720px",
            fields: "9px"
        },
        xs: {
            width: "576px",
            fields: "9px"
        },
        xxs: {
            width: "380px",
            fields: "9px"
        }
    },
    oldSizeStyle: false,
    properties: [
        'justify-content'
    ]
};

smartgrid('./css/vendors', settings);
