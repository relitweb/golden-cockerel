const gulp = require('gulp');
const concat = require("gulp-concat");
const concatCss = require("gulp-concat-css");
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const browserSync = require('browser-sync').create();
const sourcemaps = require('gulp-sourcemaps');
const preproc = require('gulp-sass');

/* gulp-group-css-media-queries */

const config = {
    src: './',
    css: {
        src: '/css/**/*.scss',
        dest: '/css',
        styleMain: '/css/main.scss'
    },
    html: {
        src: 'index.html'
    }
};

gulp.task('build', function(){
    gulp.src(config.src + config.css.styleMain)
        .pipe(preproc())
        .pipe(sourcemaps.init())
        .pipe(autoprefixer({
            browsers: ['> 0.1%'],
            cascade: false
        }))
        .pipe(cleanCSS())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(config.src + config.css.dest))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('watch', ['browserSync'], function(){
    gulp.watch(config.src + config.css.src, ['build']);
    gulp.watch(config.src + config.html.src, browserSync.reload);
});

gulp.task('browserSync', function(){
    browserSync.init({
        server: {
            baseDir: config.src
        }
    });
});
